<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Toko extends CI_Model{
    public function getToko(){
        $arr_toko[] = array(1,'Kayu',100,20000);
        $arr_toko[] = array(2,'Batu Bata',500,3000);
        $arr_toko[] = array(3,'Semen',58,20000);
        $arr_toko[] = array(4,'Keramik',120,80000);
        $arr_toko[] = array(5,'Paku',200,2000);
        $arr_toko[] = array(6,'Cat',18,35000);
        $arr_toko[] = array(7,'Pipa PVC Wavin D 1.50',100,29000);
        $arr_toko[] = array(8,'Tripleks',50,95000);
        $arr_toko[] = array(9,'Kayu Meranti',150,35000);
        $arr_toko[] = array(10,'Asbes',200,75000);
        $arr_toko[] = array(11,'Kayu Kamper',150,80000);
        $arr_toko[] = array(12,'Besi Beton',50,100000);
        $arr_toko[] = array(13,'Batu Bata Jumbo',500,1500);
        $arr_toko[] = array(14,'Pipa PVC Wavin D 4.00',100,89000);
        $arr_toko[] = array(15,'Genteng Tanah Liat Mintili',100,89000);
        $arr_toko[] = array(16,'Genteng Beton Moenir Flat',300,12500);
        $arr_toko[] = array(17,'Genteng Tanah Liat Plentong',300,1350);
        $arr_toko[] = array(18,'Genteng Beton Rabi Flat',200,11000);
        $arr_toko[] = array(19,'Kayu Borneo',150,14000);
        $arr_toko[] = array(20,'Bata Ringan',100,750000);
        $arr_toko[] = array(21,'Semen',250,80000);
        $arr_toko[] = array(22,'Cat Kayu',350,150000);
        $arr_toko[] = array(23,'Kusen Pintu',70,250000);
        $arr_toko[] = array(24,'Batu Kali',100,300000);
        $arr_toko[] = array(25,'Bata Koral',100,350000);
        $arr_toko[] = array(26,'Sekrup',150,5000);
        $arr_toko[] = array(27,'Tripleks Tebal',100,100000);
        $arr_toko[] = array(28,'Kusen Jendela',80,250000);
        $arr_toko[] = array(29,'Baut',150,10000);
        $arr_toko[] = array(30,'Engsel',100,15000);
        

        return $arr_toko;
    }
}